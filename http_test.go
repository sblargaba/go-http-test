package main

import (
	"github.com/rs/zerolog"
	"os"
	"testing"
)

func TestIsMultiple(t *testing.T) {
	tables := []struct {
		n uint64
		p []uint64
		r bool
	}{
		{2, []uint64{2}, false},
		{4, []uint64{2}, true},
		{3, []uint64{2}, false},
		{5, []uint64{2, 3}, false},
		{8, []uint64{2, 3}, true},
		{19, []uint64{2, 3}, false},
		{27, []uint64{2, 3}, true},
		{19, []uint64{2, 3, 5, 7, 11, 17, 19}, false},
		{27, []uint64{2, 3, 5, 7, 11, 17, 19}, true},
	}

	for _, table := range tables {
		out := isMultiple(table.n, table.p)
		if out != table.r {
			t.Errorf("Expected %t, got %t - n: %d, primes: %v",
				table.r, out, table.n, table.p)
		}
	}
}

func TestNthPrime(t *testing.T) {
	tables := []struct {
		n int
		r uint64
	}{
		{0, 1},
		{1, 2},
		{10, 29},
		{10001, 104743},
	}

	for _, table := range tables {
		out := nthPrime(table.n)
		if out != table.r {
			t.Errorf("Expected %d, got %d", table.r, out)
		}
	}
}

func TestGetLogLevel(t *testing.T) {
	tables := []struct {
		s string
		l zerolog.Level
	}{
		{"asdasd", zerolog.DebugLevel},
		{"debug", zerolog.DebugLevel},
		{"info", zerolog.InfoLevel},
		{"warn", zerolog.WarnLevel},
	}

	for _, table := range tables {
		out := getLogLevel(table.s)
		if out != table.l {
			t.Errorf("Expected %s, got %s", table.l.String(), out.String())
		}
	}
}

func TestGetN(t *testing.T) {
	tables := []struct {
		n string
		r int
	}{
		{"5", 5},
		{"10000", 10000},
		{"patata", 10},
		{"10", 10},
	}

	for _, table := range tables {
		out := getN(table.n)
		if out != table.r {
			t.Errorf("Expected %d, got %d", table.r, out)
		}
	}
}

func TestGetPrime(t *testing.T) {
	tables := []struct {
		n int
	}{
		{5},
		{10},
		{10000},
	}

	for _, table := range tables {
		out, _ := getPrime(table.n)
		expected := nthPrime(table.n)
		if out != expected {
			t.Errorf("Expected %d, got %d", expected, out)
		}
	}
}

func TestMain(m *testing.M) {
	// Improve performances
	zerolog.SetGlobalLevel(zerolog.Disabled)

	os.Exit(m.Run())
}
