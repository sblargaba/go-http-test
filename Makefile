default: deps test build

deps:
	@echo "=> Installing dependencies"
	go get github.com/mattn/go-isatty
	go get github.com/rs/zerolog
	go get github.com/rs/zerolog/log

build: deps
	@echo "=> Building application"
	go build -o http

test:
	@echo "=> Running tests"
	go test -cover
