FROM golang:alpine AS builder

RUN apk --no-cache add make git

WORKDIR /go/src/webserver
COPY http.go Makefile /go/src/webserver/
RUN make build

FROM alpine:latest

WORKDIR /usr/bin
COPY --from=builder /go/src/webserver/http /usr/bin/http

CMD http
