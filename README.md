# go-http-test
This is a simple project used to stress test a machine

It provides an http server on port 8080 which calculates the Nth prime number
(a generalization of Project Euler [problem #7](https://projecteuler.net/problem=7))
in order to generate load

The default returns the 10th prime number, a bigger one can be provided as
as a URL

Example: `http://localhost:8080/100`
```
100: 541
Request served in 25ms
```

The first line specify `N` and the Nth prime, the second one the milliseconds
needed to serve the request

In order to further increase load the server uses heavy logging by default: this
behaviour is configurable trough the URL

Example: `http://localhost:8080/info/10000`
```
10000: 104729
Request served in 52ms
```

Allowed values are `debug`, `info` and `warn`. If no value or the value cannot
be interpreted, the server defautls to `debug`

