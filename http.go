package main

import (
	"fmt"
	"github.com/mattn/go-isatty"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"math"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// isMultiple takes a given number and checks if it is a multiple of
// any prime contained in an ordered slice of primes
func isMultiple(number uint64, primes []uint64) bool {
	for _, prime := range primes {
		log.Debug().Msgf("Testing %d on %d", prime, number)
		if prime*prime > number {
			// number is not multiple of any of the primes in the array
			return false
		}
		if number%prime == 0 {
			// number is multiple of a prime
			return true
		}
	}
	return false
}

// nthPrime calculates and returns the nth prime number
// Generalization of Project Euler #7 https://projecteuler.net/problem=7
func nthPrime(n int) uint64 {
	// Special cases
	if n == 0 {
		return uint64(1)
	}
	if n == 1 {
		return uint64(2)
	}

	// initialize an array of size n
	primes := make([]uint64, n)
	primes[0] = 2

	var number uint64
	var length int

	for number = 3; number < math.MaxUint64; number += 2 {
		// If no dividers found
		if !isMultiple(number, primes) {
			// Increment count and append number to primes
			length++
			primes[length] = number
			// Stop if we found our nth prime
			if length+1 == n {
				break
			}
		}
	}

	// Numbers got too big, return greatest prime found
	return primes[length]
}

// getLogLevel translates a string to a zerolog.Level
func getLogLevel(levelStr string) zerolog.Level {
	switch levelStr {
	case "info":
		return zerolog.InfoLevel
	case "warn":
		return zerolog.WarnLevel
	default:
		return zerolog.DebugLevel
	}

}

// getN translates a string to a int, providing a default
// value of 10 if the conversion returns an error
func getN(nStr string) int {
	n, err := strconv.Atoi(nStr)
	if err != nil {
		n = 10
	}
	return n
}

// getPrime is a wrapper around nthPrime to provide time
// measurement
func getPrime(n int) (uint64, time.Duration) {
	start := time.Now()
	prime := nthPrime(n)
	end := time.Now()
	elapsed := end.Sub(start)
	return prime, elapsed
}

func handler(w http.ResponseWriter, r *http.Request) {
	path := strings.Split(r.URL.Path, "/")

	logLevel := getLogLevel(path[len(path)-2])
	zerolog.SetGlobalLevel(logLevel)

	n := getN(path[len(path)-1])

	log.Info().Msgf("Starting to serve with n=%d", n)
	prime, elapsed := getPrime(n)
	fmt.Fprintf(w, "%d: %d\nRequest served in %dms", n, prime, elapsed/time.Millisecond)
	log.Info().Msgf("Request served in %dms", elapsed/time.Millisecond)
}

func main() {
	zerolog.TimeFieldFormat = ""

	if isatty.IsTerminal(os.Stdout.Fd()) {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})
	} else {
		log.Logger = zerolog.New(os.Stdout).With().Timestamp().Logger()
	}

	http.HandleFunc("/", handler)
	log.Info().Msg("Listening on :8080...")
	//http.ListenAndServe(":8080", nil)
	log.Fatal().Err(http.ListenAndServe(":8080", nil)).Msg("server failed")
}
